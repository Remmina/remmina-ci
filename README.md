# remmina-ci 

# Notes

Remmina project ID: 7153509

https://gitlab.com/api/v4/projects/Remmina%2FRemmina
https://gitlab.com/api/v4/projects/7153509/

## Create an issue

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
    https://gitlab.example.com/api/v4/projects/7153509/issues?title=New%20translations%20available&labels=translations \
    -d assignee_ids=antenore \
    -d assignee_ids=giox069
```